package sheridan;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;

/*
 * @Author Vinayak Pavate
 * Student ID: 991548217
 */
public class CelsiusTest {


	@Test
	public void testFromFahrenheitRegular() {
		 int number = Celsius.fromFahrenheit(100);
	        assertTrue("Invalid input", number == 37);
	}
	@Test
	public void testFromFahrenheitException() {
		 int number = Celsius.fromFahrenheit(100);
		 assertFalse("Invalid",number==0.0);
		 
	}

	@Test
	public void testFromFahrenheitBoundaryIn() {
		 int number = Celsius.fromFahrenheit(-5);
	        assertTrue("Invalid input", number == -20);
		
	}
	@Test
	public void testFromFahrenheitBoundaryOut() {
		int number = Celsius.fromFahrenheit(-6);
        assertTrue("Invalid input", number == -21);
	
		
	}

}
