package sheridan;
/*
 * @Author Vinayak Pavate
 * Student ID: 991548217
 */
public class Celsius {
	 public static int fromFahrenheit(int fahrenheit) {
		int celsius =  (int) ((fahrenheit - 32) / 1.8) ;
		return celsius;
	    }
}
